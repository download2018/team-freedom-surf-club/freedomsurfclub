import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { SignupPage } from '../signup/signup';

import { UtenteProvider } from '../../providers/utente/utente';
import { TabsPage } from '../tabs/tabs';
import { UserData } from '../../providers/user-data';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: string;
  password: string; 
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public alertCtrl: AlertController, 
    public utenteProvider: UtenteProvider,
    public userData: UserData) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signup() {
    this.navCtrl.push(SignupPage);
  }

  login() {
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isEmailValid = emailRegex.test(this.email);
    let passwordRegex = /^[a-zA-Z\d]{8,20}$/;
    let isPasswordValid = passwordRegex.test(this.password);
    if(!isEmailValid)
    {
      let alert = this.alertCtrl.create({
        title: 'Email non valida',
        subTitle: "L'email inserita non è valida",
        buttons: ['Chiudi']
      });
      alert.present();
    }
    else if(!isPasswordValid)
    {
      let alert = this.alertCtrl.create({
        title: "Formato password errato",
        buttons: ['Chiudi']
      });
      alert.present();
    }
    this.utenteProvider.login(this.email, this.password).then(json => {
      var a = JSON.parse(json["_body"]);
      if (a["id"] != undefined) {
        this.userData.setUser(a["id"]);
        this.userData.setNome(a["nome"] + " " + a["cognome"]);
        this.navCtrl.push(TabsPage);
      } else {
        let alert = this.alertCtrl.create({
          title: "Accesso non consentito",
          buttons: ['Chiudi']
        });
        alert.present();
      }
    })
  }
}
