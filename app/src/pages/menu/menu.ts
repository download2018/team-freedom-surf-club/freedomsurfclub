import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { ListPage } from '../list/list';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  nome: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public userData: UserData
  ) {
  }

  ionViewDidLoad() {
    this.userData.getNome().then((nome) => {
      this.nome = nome;
    });
  }

  goToList() {
    this.navCtrl.push(ListPage);
  }

}
