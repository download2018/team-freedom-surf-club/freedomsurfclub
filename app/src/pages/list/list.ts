import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Segnalazione, SegnalazioniProvider } from '../../providers/segnalazioni/segnalazioni';
import { UserData } from '../../providers/user-data';

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {

  segnalazioni: Array<Segnalazione>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public segnalazioniProvider: SegnalazioniProvider,
    public userData: UserData)  {
    
  }

  ionViewDidLoad() {
    this.userData.getUser().then((idUser) => {
      this.segnalazioniProvider.getSegnalazioniUtente(idUser).then(data => {
        console.log(data);
        this.segnalazioni = data;
        console.log(this.segnalazioni);
      });
    });
  }

  appogiaSegnalazione(id) {
    this.segnalazioniProvider.appoggiaSegnalazione(id, 1);
  }
}
