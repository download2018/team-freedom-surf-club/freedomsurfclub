import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';

import { Segnalazione, SegnalazioniProvider } from '../../providers/segnalazioni/segnalazioni';
import { AddAlertPage } from '../add-alert/add-alert';
 
declare var google;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  mylat: number;
  mylng: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public geolocation: Geolocation, public segnalazioniProvider: SegnalazioniProvider,
    public alertCtrl: AlertController) { 

      this.mylat = 0;
      this.mylng = 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.getCurrentPosition().then(() => {
      this.loadMap();
    }).catch(e => console.log(e));

  }

  getCurrentPosition() {
    return new Promise(resolve => {
      this.geolocation.getCurrentPosition().then((position) => {
        this.mylat = position.coords.latitude;
        this.mylng = position.coords.longitude;
        resolve(true);
      }).catch((err) => {
        console.log(err);
      });
    });
  }
 
  loadMap(){
 
    let latLng = new google.maps.LatLng(this.mylat, this.mylng);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    let lat = this.mylat;
    let lng = this.mylng;
    let message = "La tua posizione";
    let icon = "";

    this.addMarker(lat, lng, message, icon);

    this.caricaSegnalazioni();
  }

  addMarker(lat, lng, message, icon) {

    if(icon!='') {
      icon = {
        url: icon,
        scaledSize: new google.maps.Size(40, 40), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
      };
    }
 
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: { lat: lat, lng: lng },
      icon: icon
    });
   
    let content = "<b>" + message + "</b>";
 
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
   
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

    google.maps.event.addListener(marker, 'dblclick', () => {
      console.log("doppio click");
      let alert = this.alertCtrl.create({
        title: 'Vuoi appoggiare questa segnalazione?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'SI',
            handler: () => {
              console.log('Buy clicked');
              this.segnalazioniProvider.appoggiaSegnalazione(marker.id_segnalazione, 1);
            }
          }
        ]
      });
      alert.present();
    });
   
  }

  caricaSegnalazioni() {
    let lat, lng, message, icon, segnalazione;

    this.segnalazioniProvider.getSegnalazioni().then(segnalazioni => {
      for(let i=0; i<segnalazioni.length; i++) {
        segnalazione = segnalazioni[i];

        lat = segnalazione.latitudine;
        lng = segnalazione.longitudine;
        message = segnalazione.sottocategoria;

        icon = "../../assets/imgs/icone/";
        icon += segnalazione.sottocategoria;
        
        if(segnalazione.id_stato == 2)
          icon += "_g";

          icon += ".png";
  
        this.addMarker(lat, lng, message, icon);
      }
    }).catch(e => console.log(e));
  }

  openAddAlertPage() {
    this.navCtrl.push(AddAlertPage);
  }
}