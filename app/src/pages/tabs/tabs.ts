import { Component } from '@angular/core';

import { MapPage } from '../map/map';
import { ListPage } from '../list/list';
import { MenuPage } from '../menu/menu';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1 = MapPage;
  tab3 = MenuPage;

  constructor() {

  }
}
