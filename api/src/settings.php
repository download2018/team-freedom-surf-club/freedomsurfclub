<?php
return [
    'settings' => [
        'displayErrorDetails' => false, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
         'db' => [   
                'host'   => 'localhost',
                'user'   => 'freedomsurfclub',
                'pass'   => '',
                'dbname' => 'my_freedomsurfclub',
            ],
        
        
    ],
];
